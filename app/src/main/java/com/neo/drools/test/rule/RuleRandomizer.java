package com.neo.drools.test.rule;

import com.neo.drools.model.domain.AssignationData;
import com.neo.drools.util.randomizer.*;
import com.neo.drools.model.domain.Rule;
import com.neo.drools.model.domain.fact.AssignationCheckResult;
import static com.neo.drools.util.randomizer.Randomizer.randomDate;
import static com.neo.drools.util.randomizer.Randomizer.randomInt;
import static com.neo.drools.util.randomizer.Randomizer.randomString;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Adriasn Novegil <adrian.novegil@gmail.com>
 */
public class RuleRandomizer extends InditexRandomizer {

    private static final String[] CATALOGOS = {"KC", "KC REGLAS", "SEN", "ABC",
        "SC-Reglas", "UNIT MEASURE", "EAC", "SGZ", "AdvertenciaUcrania",
        "Cosmetica Ucrania ***", "Cuberteria Serbia", "TRIMAN - FRANCIA",
        "TEXTIL VIETNAM", "KAFF (KEEP AWAY FROM FIRE)", "DGR (MERCANCIA PELIGROSA)",
        "VOLUMINOSO", "BEAUTY (COSMETICA)"};

    /**
     * Generate a new Rule content
     *
     * @return the rule content
     */
    public static String randomRuleContent() {
        return "package com.neo.drools\n"
                + "\n"
                + "import com.neo.drools.model.domain.AssignationData;\n"
                + "import com.neo.drools.model.domain.fact.AssignationCheckResult;\n"
                + "\n"
                + "rule \"Postcode " + randomInt(10, 100) + " numbers\"\n"
                + "\n"
                + "    when\n"
                + "        assignationData : AssignationData()\n"
                + "        checkResult : AssignationCheckResult();\n"
                + "    then\n"
                + "        checkResult.setPostCodeResult(true);\n"
                + "        System.out.println(\"Registro：OK!\");\nend";
    }

    /**
     * Generate a new instance of Rule class
     *
     * @return the new Rule
     */
    public static Rule randomRule() {
        Rule rule = new Rule();
        // private String ruleKey;
        rule.setRuleKey(randomString(2, 12));
        // private String content;
        rule.setContent(randomRuleContent());
        // private String catalog;
        rule.setCatalog(randomCatalogo());
        // private String version;    
        rule.setVersion(randomString(2, 5));
        // private String lastModifyTime;
        rule.setLastModifyTime(randomDate().toString());
        // private String createTime;
        rule.setCreateTime(randomDate().toString());
        return rule;
    }

    /**
     * Generate a new list of Rule class
     *
     * @param numElements number of rules we want to generate
     * @return the list of new instances
     */
    public static List<Rule> randomRule(int numElements) {
        List<Rule> rules;
        rules = new ArrayList<>();
        for (int i = 0; i < numElements; i++) {
            rules.add(randomRule());
        }
        return rules;
    }

    /**
     *
     * @return
     */
    public static AssignationData randomAssignationData() {
        AssignationData assignationData = new AssignationData();
        assignationData.setPostcode(randomString(5, 5));
        assignationData.setState(randomString(5, 12));
        assignationData.setStreet(randomString(24, 36));
        return assignationData;
    }

    /**
     *
     * @param numElements
     * @return the list of elements
     */
    public static List<AssignationData> randomAssignationData(int numElements) {
        List<AssignationData> assignationDatas;
        assignationDatas = new ArrayList<>();
        for (int i = 0; i < numElements; i++) {
            assignationDatas.add(randomAssignationData());
        }
        return assignationDatas;
    }

    /**
     *
     * @return
     */
    public static AssignationCheckResult randomAssignationCheckResult() {
        AssignationCheckResult assignationCheckResult = new AssignationCheckResult();
        assignationCheckResult.setPostCodeResult(randomBoolean());
        return assignationCheckResult;
    }

    /**
     *
     * @param numElements
     * @return the list of elements
     */
    public static List<AssignationCheckResult> randomAssignationCheckResult(int numElements) {
        List<AssignationCheckResult> assignationCheckResults;
        assignationCheckResults = new ArrayList<>();
        for (int i = 0; i < numElements; i++) {
            assignationCheckResults.add(randomAssignationCheckResult());
        }
        return assignationCheckResults;
    }

    /**
     *
     * @return
     */
    public static String randomCatalogo() {
        return CATALOGOS[randomInt(0, CATALOGOS.length - 1)];
    }
}
