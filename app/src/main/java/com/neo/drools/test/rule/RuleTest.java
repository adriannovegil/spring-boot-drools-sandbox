package com.neo.drools.test.rule;

import com.neo.drools.model.domain.fact.AssignationCheckResult;
import com.neo.drools.util.test.Test;
import com.neo.drools.model.domain.Rule;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.neo.drools.service.RuleService;

/**
 *
 * @author Adrian Novegil <adrian.novegil@gmail.com>
 */
public class RuleTest extends Test {

    // Looger del test
    private static final Logger logger = LoggerFactory.getLogger(RuleTest.class);

    private final List<Object> facts = new ArrayList<>();

    private final RuleService ruleService;
    private final Integer numArticles;
    private final Integer numRules;

    /**
     * Constructor por defecto.
     *
     * @param ruleService
     * @param numArticles
     * @param numRules
     */
    public RuleTest(RuleService ruleService, Integer numArticles, Integer numRules) {
        this.ruleService = ruleService;
        // Seteamos el nombre del test
        this.setName("Rules Test");
        this.numArticles = numArticles;
        this.numRules = numRules;
    }

    /**
     * Metodo que se encarga de configurar el entorno de ejecucion, variables de
     * entorno, factorías, etc.
     *
     * @return True si el metodo se ha ejecutado correctamente, false en caso
     * contrario.
     */
    @Override
    public Boolean config() {
        // Limpiamos la base de datos de pruebas por si ha habido un error
        // en la ejecucion. Evitamos posibles interferencias de ejecuciones previas
        this.ruleService.deleteAll();
        // Generamos un conjunto de reglas aleatorias en base a la configuracion
        // de la prueba que queremos hacer
        List<Rule> rules = new ArrayList();
        for (int i = 0; i < this.numRules; i++) {
            rules.add(RuleRandomizer.randomRule());
        }
        // Save all the rules
        this.ruleService.save(rules);
        // Cargamos en el contexto de Drools las reglas que queremos evaluar.
        this.ruleService.loadRulesByCatalog("KC");
//        this.ruleService.loadRules();
        // Retornamos true indicando que el metodo ha terminado correctamente
        return true;
    }

    /**
     * Metodo que se encarga de ejecutar todas las acciones necesarias para
     * preparar el contexto del test.
     *
     * @return True si el metodo se ha ejecutado correctamente, false en caso
     * contrario.
     */
    @Override
    public Boolean prepare() {
        // Prepare the test facts
        for (int i = 0; i < this.numArticles; i++) {
            this.facts.add(
                    RuleRandomizer.randomAssignationData());
        }
        AssignationCheckResult result = new AssignationCheckResult();
        this.facts.add(result);
        // Retornamos true indicando que el metodo ha terminado correctamente
        return true;
    }

    /**
     * Metodo que ejecuta el core de la prueba que estamos realizando.
     *
     * @return True si el metodo se ha ejecutado correctamente, false en caso
     * contrario.
     */
    @Override
    public Boolean execute() {
        // Execute the rules
        this.ruleService.execute(this.facts);
        // Todo ha salido perfectamente.
        return true;
    }

    /**
     * Metodo encargado de finalizar la ejecucion del test, cierre de contextos,
     * etc.
     *
     * @return True si el metodo se ha ejecutado correctamente, false en caso
     * contrario.
     */
    @Override
    public Boolean close() {
        // Limpiamos la base de datos de pruebas por si ha habido un error
        // en la ejecucion. Evitamos posibles interferencias de ejecuciones previas
        this.ruleService.deleteAll();
        // Indicamos que todo ha sucedido correctamente.
        return true;
    }
}
