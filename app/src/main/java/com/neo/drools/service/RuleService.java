package com.neo.drools.service;

import com.neo.drools.model.domain.Rule;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author Adrian Novegil <adrian.novegil@gmail.com>
 */
public interface RuleService {

    /**
     * Save a rule.
     *
     * @param rule the entity to save
     * @return the persisted entity
     */
    Rule save(Rule rule);

    /**
     *
     * @param rule
     * @return
     */
    List<Rule> save(List<Rule> rule);

    /**
     * Get the "id" rule.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<Rule> find(Long id);

    /**
     * Get the rules based on the catalog
     *
     * @param catalog the rules catalog
     * @return the list of rules
     */
    List<Rule> findByCatalog(String catalog);

    /**
     * Get all the rules.
     *
     * @return the list of entities
     */
    List<Rule> findAll();

    /**
     * Delete the "id" rule.
     *
     * @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Delete the all the rules.
     *
     */
    void deleteAll();

    /**
     * Evaluate a list of facts
     *
     * @param objects facts we want to evaluateF
     */
    public void execute(List<Object> objects);

    /**
     * Load the engine rules fron the database
     */
    public void loadRules();

    /**
     * Load the engine rules in a catalog fron the database
     *
     * @param catalog rules catalog
     */
    public void loadRulesByCatalog(String catalog);

}
