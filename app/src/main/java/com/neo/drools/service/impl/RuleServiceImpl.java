package com.neo.drools.service.impl;

import com.neo.drools.model.domain.Rule;
import com.neo.drools.model.repository.RuleRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import org.springframework.transaction.annotation.Transactional;
import com.neo.drools.util.ruleengine.KieUtils;
import com.neo.drools.util.ruleengine.RuleEngine;
import org.kie.api.KieServices;
import org.kie.api.builder.KieBuilder;
import org.kie.api.builder.KieFileSystem;
import org.kie.api.builder.KieRepository;
import org.kie.api.builder.Message;
import org.kie.api.runtime.KieContainer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.neo.drools.service.RuleService;
import java.util.Optional;

/**
 *
 * @author Adrian Novegil <adrian.novegil@gmail.com>
 */
@Service
@Transactional
public class RuleServiceImpl implements RuleService {

    private final Logger logger = LoggerFactory.getLogger(RuleServiceImpl.class);

    private final RuleEngine ruleEngine;
    private final RuleRepository ruleRepository;

    /**
     * Class constructor
     *
     * @param ruleEngine
     * @param ruleRepository
     */
    public RuleServiceImpl(RuleEngine ruleEngine, RuleRepository ruleRepository) {
        this.ruleEngine = ruleEngine;
        this.ruleRepository = ruleRepository;
    }

    /**
     * Save a rule
     *
     * @param rule the rule
     * @return new saved rule
     */
    @Override
    public Rule save(Rule rule) {
        logger.debug("Request to save Rule : {}", rule);
        rule = ruleRepository.save(rule);
        return rule;
    }

    /**
     * Save a rule
     *
     * @param rules
     * @return new saved rule
     */
    @Override
    public List<Rule> save(List<Rule> rules) {
        logger.debug("Request to save all Rules : {}", rules);
        rules = ruleRepository.saveAll(rules);
        return rules;
    }

    /**
     * Find a rule by id
     *
     * @param id id of the rule
     * @return the rule
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<Rule> find(Long id) {
        logger.debug("Request to get Rule : {}", id);
        return ruleRepository.findById(id);
    }

    /**
     * Get the rules based on the catalog
     *
     * @param catalog the rules catalog
     * @return the list of rules
     */
    @Override
    @Transactional(readOnly = true)
    public List<Rule> findByCatalog(String catalog) {
        logger.debug("Request to get all Rules in a catalog");
        return ruleRepository.findByCatalog(catalog);
    }

    /**
     * Get all the rules
     *
     * @return the rules
     */
    @Override
    @Transactional(readOnly = true)
    public List<Rule> findAll() {
        logger.debug("Request to get all Rules");
        return ruleRepository.findAll();
    }

    /**
     * Delete one rule by id
     *
     * @param id id of the rule we want to delete
     */
    @Override
    public void delete(Long id) {
        logger.debug("Request to delete Rule : {}", id);
        ruleRepository.deleteById(id);
    }

    /**
     * Delete all rules
     */
    @Override
    public void deleteAll() {
        logger.debug("Request to delete all Rules");
        ruleRepository.deleteAll();
    }

    /**
     *
     * @param objects
     */
    @Override
    public void execute(List<Object> objects) {
        this.ruleEngine.execute(objects);
    }

    /**
     * Load the engine rules fron the database
     */
    @Override
    public void loadRules() {
        KieUtils.setKieContainer(loadContainerFromString(getRules()));
    }

    /**
     * Load the engine rules fron the database
     *
     * @param catalog
     */
    @Override
    public void loadRulesByCatalog(String catalog) {        
        KieUtils.setKieContainer(loadContainerFromString(
                ruleRepository.findByCatalog(catalog)));
    }

    /**
     * Get the rules for the experiment from the database
     *
     * @return
     */
    private List<Rule> getRules() {
        List<Rule> rules = ruleRepository.findAll();
        return rules;
    }

    /**
     *
     * @param rules
     * @return
     */
    private KieContainer loadContainerFromString(List<Rule> rules) {
        long startTime = System.currentTimeMillis();
        KieServices ks = KieServices.Factory.get();
        KieRepository kr = ks.getRepository();
        // First, we need to set the KieFileSystem bean; this is an in-memory 
        // file system provided by the framework. Following code provides the 
        // container to define the Drools resources like rules files, decision 
        // tables, programmatically:
        KieFileSystem kfs = ks.newKieFileSystem();
        for (Rule rule : rules) {
            String drl = rule.getContent();
            kfs.write(KieUtils.RULES_PATH + drl.hashCode() + ".drl", drl);
        }
        // Next, we need to set the KieContainer which is a placeholder for all 
        // the KieBases for particular KieModule. KieContainer is built with the 
        // help of other beans including KieFileSystem, KieModule, and KieBuilder.
        KieBuilder kb = ks.newKieBuilder(kfs);
        // The buildAll() method invoked on KieBuilder builds all the resources 
        // and ties them to KieBase. 
        // It executes successfully only when it’s able to find and validate all 
        // of the rule files:
        kb.buildAll();
        if (kb.getResults().hasMessages(Message.Level.ERROR)) {
            throw new RuntimeException("Build Errors:\n" + kb.getResults().toString());
        }
        long endTime = System.currentTimeMillis();
        System.out.println("Time to build rules : " + (endTime - startTime) + " ms");
        // Generate a new kie container
        startTime = System.currentTimeMillis();
        KieContainer kContainer = ks.newKieContainer(kr.getDefaultReleaseId());
        endTime = System.currentTimeMillis();
        System.out.println("Time to load container: " + (endTime - startTime) + " ms");
        // Return the new container
        return kContainer;
    }
}
