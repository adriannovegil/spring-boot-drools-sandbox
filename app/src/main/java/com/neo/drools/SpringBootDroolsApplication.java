package com.neo.drools;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 *
 * @author Adrian Novegil <adrian.novegil@gmail.com>
 */
@SpringBootApplication
@EnableJpaRepositories("com.neo.drools.model.repository")
public class SpringBootDroolsApplication {

    /**
     *
     * @param args
     */
    public static void main(String[] args) {
        SpringApplication.run(SpringBootDroolsApplication.class, args);
    }

}
