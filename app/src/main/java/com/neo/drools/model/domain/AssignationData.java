package com.neo.drools.model.domain;

/**
 *
 * @author Adrian Novegil <adrian.novegil@gmail.com>
 */
public class AssignationData {

    private String postcode;
    private String street;
    private String state;

    /**
     * 
     * @return 
     */
    public String getPostcode() {
        return postcode;
    }

    /**
     * 
     * @param postcode 
     */
    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    /**
     * 
     * @return 
     */
    public String getStreet() {
        return street;
    }

    /**
     * 
     * @param street 
     */
    public void setStreet(String street) {
        this.street = street;
    }

    /**
     * 
     * @return 
     */
    public String getState() {
        return state;
    }

    /**
     * 
     * @param state 
     */
    public void setState(String state) {
        this.state = state;
    }

}
