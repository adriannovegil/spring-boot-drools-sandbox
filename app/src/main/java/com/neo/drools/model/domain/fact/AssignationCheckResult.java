package com.neo.drools.model.domain.fact;

/**
 *
 * @author Adrian Novegil <adrian.novegil@gmail.com>
 */
public class AssignationCheckResult {

    private boolean postCodeResult = false;

    /**
     * 
     * @return 
     */
    public boolean isPostCodeResult() {
        return postCodeResult;
    }

    /**
     * 
     * @param postCodeResult 
     */
    public void setPostCodeResult(boolean postCodeResult) {
        this.postCodeResult = postCodeResult;
    }
}
