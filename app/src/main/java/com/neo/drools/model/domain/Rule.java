package com.neo.drools.model.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;

/**
 *
 * @author Adrian Novegil <adrian.novegil@gmail.com>
 */
@Entity
public class Rule implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue
    private Long id;
    @Column(nullable = false, unique = true)
    private String ruleKey;
    @Column(nullable = false)
    private String content;
    @Column(nullable = false)
    private String catalog;
    @Column(nullable = false, unique = true)
    private String version;
    @Column(nullable = true, unique = true)
    private String lastModifyTime;
    @Column(nullable = false)
    private String createTime;

    /**
     *
     */
    public Rule() {
    }

    /**
     *
     * @param ruleKey
     * @param content
     * @param catalog
     * @param version
     * @param lastModifyTime
     * @param createTime
     */
    public Rule(String ruleKey, String content, String catalog, String version,
            String lastModifyTime, String createTime) {
        this.ruleKey = ruleKey;
        this.content = content;
        this.catalog = catalog;
        this.version = version;
        this.lastModifyTime = lastModifyTime;
        this.createTime = createTime;
    }

    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public String getRuleKey() {
        return ruleKey;
    }

    /**
     *
     * @param ruleKey
     */
    public void setRuleKey(String ruleKey) {
        this.ruleKey = ruleKey;
    }

    /**
     *
     * @return
     */
    public String getContent() {
        return content;
    }

    /**
     *
     * @param content
     */
    public void setContent(String content) {
        this.content = content;
    }

    /**
     *
     * @return
     */
    public String getCatalog() {
        return catalog;
    }

    /**
     *
     * @param catalog
     */
    public void setCatalog(String catalog) {
        this.catalog = catalog;
    }

    /**
     *
     * @return
     */
    public String getVersion() {
        return version;
    }

    /**
     *
     * @param version
     */
    public void setVersion(String version) {
        this.version = version;
    }

    /**
     *
     * @return
     */
    public String getLastModifyTime() {
        return lastModifyTime;
    }

    /**
     *
     * @param lastModifyTime
     */
    public void setLastModifyTime(String lastModifyTime) {
        this.lastModifyTime = lastModifyTime;
    }

    /**
     *
     * @return
     */
    public String getCreateTime() {
        return createTime;
    }

    /**
     *
     * @param createTime
     */
    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
