package com.neo.drools.model.repository;

import com.neo.drools.model.domain.Rule;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author Adrian Novegil <adrian.novegil@gmail.com>
 */
public interface RuleRepository extends JpaRepository<Rule, Long> {

    /**
     *
     * @param ruleKey
     * @return
     */
    Rule findByRuleKey(String ruleKey);

    /**
     *
     * @param catalog
     * @return
     */
    List<Rule> findByCatalog(String catalog);
}
