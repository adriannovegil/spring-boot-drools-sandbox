package com.neo.drools.web.rest;

import java.io.IOException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.neo.drools.service.RuleService;

/**
 *
 * @author Adrian Novegil <adrian.novegil@gmail.com>
 */
@RestController
@RequestMapping("/api")
public class RuleController {

    private final RuleService reloadDroolsRulesService;

    /**
     * Class constructor
     *
     * @param reloadDroolsRulesService
     */
    public RuleController(RuleService reloadDroolsRulesService) {
        this.reloadDroolsRulesService = reloadDroolsRulesService;
    }

    /**
     *
     * @return @throws IOException
     */
    @GetMapping("/rules/reload")
    public String reload() throws IOException {
        this.reloadDroolsRulesService.loadRules();
        return "ok";
    }

}
