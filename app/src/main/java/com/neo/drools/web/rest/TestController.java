package com.neo.drools.web.rest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.neo.drools.test.rule.RuleTest;
import com.neo.drools.util.test.TestExecutor;
import com.neo.drools.service.RuleService;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author Adrian Novegil <adrian.novegil@gmail.com>
 */
@RestController
@RequestMapping("/api")
public class TestController {

    private final RuleService reloadDroolsRulesService;

    /**
     * Class contructor
     *
     * @param reloadDroolsRulesService
     */
    public TestController(RuleService reloadDroolsRulesService) {
        this.reloadDroolsRulesService = reloadDroolsRulesService;
    }

    /**
     * Rules test endpoint
     *
     * @param numArticles
     * @param numRules
     */
    @GetMapping("/tests/rules")
    public void testRules(@RequestParam("numArticles") Integer numArticles,
            @RequestParam("numRules") Integer numRules) {
        // Validate params
        numArticles = (numArticles == null) ? 0 : numArticles;
        numRules = (numRules == null) ? 0 : numRules;
        // Creamos una instancia del test a ejecutar.
        RuleTest ruleTest = new RuleTest(this.reloadDroolsRulesService,
        numArticles, numRules);
        // Ejecutamos y seteamos en el mapa de resultados el nombre del
        // test y el resultado obtenido.
        TestExecutor.process(ruleTest);
    }

}
