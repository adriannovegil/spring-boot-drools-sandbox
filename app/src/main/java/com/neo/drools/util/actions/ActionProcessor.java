package com.neo.drools.util.actions;

import com.neo.drools.util.Constants;
import com.neo.drools.util.exceptions.InternalErrorException;
import com.neo.drools.util.exceptions.ModelException;

/**
 *
 * @author Adrian Novegil <adrian.novegil@gmail.com>
 */
public final class ActionProcessor {

    /**
     * Consturctor por defecto.
     */
    private ActionProcessor() {
    }

    /**
     *
     * @param <T>
     * @param action
     * @return
     * @throws ModelException
     * @throws InternalErrorException
     */
    public static <T> T process(NonTransactionalAction<T> action)
            throws ModelException, InternalErrorException {
        try {
            //Retornamos el objeto resultado de la ejecucion de la accion.
            return action.execute();
        } catch (InternalErrorException e) {
            //En caso de detectar un error lo controlamos y anotamos la incidencia.
            //TODO: Meter la logica de auditoria en esta seccion
            throw new InternalErrorException(e);
        }
    }

    /**
     *
     * @param <T>
     * @param action
     * @return
     * @throws ModelException
     * @throws InternalErrorException
     */
    public static <T> T process(TransactionalAction<T> action)
            throws ModelException, InternalErrorException {
        boolean commited = false;
        try {
            //TODO: Falta código para controla posibles errores.
            //Preparamos el contexto de ejecución
            action.prepare();
            //Retornamos el objeto resultado de la ejecucion de la accion.            
            T resultado = action.execute();
            //Cerramos la transación
            action.done(Constants.VOTE_COMMIT);
            //TODO: Comentar
            commited = true;
            //TODO: Comentar
            return resultado;
        } catch (ModelException | InternalErrorException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalErrorException(e);
        } finally {
            try {
                if (action != null) {
                    if (!commited) {
                        action.done(Constants.VOTE_ROLLBACK);
                    }
                }
            } catch (Exception e) {
                throw new InternalErrorException(e);
            }
        }
    }
}
