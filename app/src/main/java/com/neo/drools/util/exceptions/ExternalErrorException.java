package com.neo.drools.util.exceptions;

/**
 *
 * @author Adrian Novegil <adrian.novegil@gmail.com>
 */
public class ExternalErrorException extends ModelException {

    /**
     *
     */
    public ExternalErrorException() {
        super();
    }

    /**
     *
     * @param s
     */
    public ExternalErrorException(String s) {
        super(s);
    }
    
    /**
     * 
     * @param exception 
     */
    public ExternalErrorException(Throwable exception) {
        super(exception);
    }
}
