package com.neo.drools.util.actions;

import com.neo.drools.util.exceptions.InternalErrorException;
import com.neo.drools.util.exceptions.ModelException;

/**
 *
 * @author Adrian Novegil <adrian.novegil@gmail.com>
 * @param <T>
 */
public interface Action<T> {
    /**
     * 
     * @return
     * @throws ModelException
     * @throws InternalErrorException 
     */
    public T execute() throws ModelException, InternalErrorException;
}
