package com.neo.drools.util.ruleengine;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.annotation.PostConstruct;
import org.kie.api.definition.KiePackage;
import org.kie.api.definition.rule.Rule;
import org.kie.api.runtime.KieSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 *
 * @author Adrian Novegil <adrian.novegil@gmail.com>
 */
@Component
public class RuleEngine {

    private final Logger logger = LoggerFactory.getLogger(RuleEngine.class);

    private KieSession kieSession;

    /**
     * Init the Kie execution environment
     */
    @PostConstruct
    public void init() {
        logger.debug("Initializing Drools engine ...");
        initKie();
    }

    /**
     * Load the knowledge base
     */
    private void initKie() {
        // Upload the Drool Files
        logger.info("Starting Drools with drl files from:  " + KieUtils.RULES_PATH);
    }

    /**
     *
     * @param objects
     */
    public void execute(List<Object> objects) {
        try {
            this.kieSession = KieUtils.getKieContainer().newKieSession();
            if (!getRulenames().isEmpty()) {
                // Load the facts
                for (Object o : objects) {
                    this.kieSession.insert(o);
                }
                int rulesFired = this.kieSession.fireAllRules();
                logger.info("Number of rules fired on event: " + rulesFired);
            } else {
                logger.info("No rules in the system - skipping firing rules");
            }
            this.kieSession.dispose();
        } catch (Exception e) {
            logger.error("Error during rules enging processing:  " + e.getMessage());
        }
    }

    /**
     *
     * @return @throws java.lang.Exception
     */
    public List<String> getRulenames() throws Exception {
        List<String> result = new ArrayList();
        try {
            KiePackage kpkg = KieUtils.getKieContainer().getKieBase().getKiePackage(KieUtils.PACKAGE_NAME);
            if (kpkg != null) {
                Collection<Rule> rules = kpkg.getRules();
                if (rules != null) {
                    for (Rule rule : rules) {
                        result.add(rule.getName());
                    }
                }
            }
            return result;
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            throw new Exception(ex);
        }
    }
}
