package com.neo.drools.util.actions;

/**
 *
 * @author Adrian Novegil <adrian.novegil@gmail.com>
 * @param <T>
 */
public interface NonTransactionalAction<T> extends Action<T> {

}
