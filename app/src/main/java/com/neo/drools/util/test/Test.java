package com.neo.drools.util.test;

import java.io.Serializable;

/**
 *
 * @author Adrian Novegil <adrian.novegil@gmail.com>
 */
public abstract class Test implements Serializable {

    private String name;
//    public static Boolean VERBOSE_MODE = null;
//    public static Boolean TEST_MODE = null;
//    public static Integer NUM_TRIALS = null;

//    static {
//        // Modo verbose.
//        VERBOSE_MODE = (ConfigurationManager.get("apache.benchmark.config.global.verbose").equals("1"));
//        // Modo test. Se ejecutan metodos de validacion de datos internos.
//        TEST_MODE = (ConfigurationManager.get("apache.benchmark.config.global.test.mode").equals("1"));
//        // Numero de repeticiones de los experimentos
//        NUM_TRIALS = new Integer(ConfigurationManager.get("apache.benchmark.config.global.num.trials"));
//    }
    /**
     * Metodo que nos permite recuperar el valor del atributo name.
     *
     * @return Valor del atributo name.
     */
    public String getName() {
        return name;
    }

    /**
     * Metodo que nos permite setear el valor del atributo name.
     *
     * @param name Nuevo valor del atributo name.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Metodo que se encarga de configurar el entorno de ejecucion, variables de
     * entorno, factorías, etc.
     *
     * @return True si el metodo se ha ejecutado correctamente, false en caso
     * contrario.
     */
    public abstract Boolean config();

    /**
     * Metodo que se encarga de ejecutar todas las acciones necesarias para
     * preparar el contexto del test.
     *
     * @return True si el metodo se ha ejecutado correctamente, false en caso
     * contrario.
     */
    public abstract Boolean prepare();

    /**
     * Metodo que ejecuta el core de la prueba que estamos realizando.
     *
     * @return True si el metodo se ha ejecutado correctamente, false en caso
     * contrario.
     */
    public abstract Boolean execute();

    /**
     * Metodo encargado de finalizar la ejecucion del test, cierre de
     * contextos, etc.
     *
     * @return True si el metodo se ha ejecutado correctamente, false en caso
     * contrario.
     */
    public abstract Boolean close();
}
