package com.neo.drools.util.exceptions;

/**
 *
 * @author Adrian Novegil <adrian.novegil@gmail.com>
 */
public class InternalErrorException extends Exception {

    /**
     *
     */
    public InternalErrorException() {
        super();
    }

    /**
     *
     * @param s
     */
    public InternalErrorException(String s) {
        super(s);
    }
    
    /**
     * 
     * @param exception 
     */
    public InternalErrorException(Throwable exception) {
        super(exception);
    }
}
