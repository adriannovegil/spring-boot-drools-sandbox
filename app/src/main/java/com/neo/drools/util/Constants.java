package com.neo.drools.util;

/**
 *
 * @author Adrian Novegil <adrian.novegil@gmail.com>
 */
public final class Constants {

    // transaction status constants
    public static final Integer STATUS_ACTIVE = 1;
    public static final Integer STATUS_MARKED_ROLLBACK = 2;
    public static final Integer STATUS_PREPARED = 3;
    public static final Integer STATUS_COMMITTED = 4;
    public static final Integer STATUS_ROLLED_BACK = 5;
    public static final Integer STATUS_UNKNOWN = 6;
    public static final Integer STATUS_NO_TTRANSACTION = 7;

    // vote constants
    public static final Integer VOTE_COMMIT = 8;
    public static final Integer VOTE_ROLLBACK = 9;
    public static final Integer VOTE_READ_ONLY = 10;

    // commitResult constants
    public static final Integer COMMIT_RESULT_COMMIT_ONE_PHASE = 11;
    public static final Integer COMMIT_RESULT_NOT_PREPARED = 12;
    public static final Integer COMMIT_RESULT_HEURISTIC_MIXED = 13;
    public static final Integer COMMIT_RESULT_HEURISTIC_ROLLBACK = 14;
    public static final Integer COMMIT_RESULT_HEURISTIC_HAZARD = 15;
    public static final Integer COMMIT_RESULT_REMOTE_EXCEPTION = 16;
    public static final Integer COMMIT_RESULT_READ_ONLY = 17;
    public static final Integer COMMIT_RESULT_COMMITTED = 18;

    // heuristicStatus constants
    public static final Integer HEURISTIC_STATUS_HAZARD = 21;
    public static final Integer HEURISTIC_STATUS_MIXED = 22;
    public static final Integer HEURISTIC_STATUS_NORMAL = 23;
    public static final Integer HEURISTIC_STATUS_ROLLBACK = 24;
    public static final Integer HEURISTIC_STATUS_COMMIT = 25;

    // commitDecision contants
    public static final Integer COMMIT_DECISION_ROLLBACK = 31;
    public static final Integer COMMIT_DECISION_COMMIT_ONE_PHASE = 32;

    private Constants() {
    }
}
