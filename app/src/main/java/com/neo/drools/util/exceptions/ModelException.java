package com.neo.drools.util.exceptions;

/**
 *
 * @author Adrian Novegil <adrian.novegil@gmail.com>
 */
public abstract class ModelException extends Exception {

    /**
     *
     */
    public ModelException() {
        super();
    }

    /**
     *
     * @param s
     */
    public ModelException(String s) {
        super(s);
    }

    /**
     *
     * @param exception
     */
    public ModelException(Throwable exception) {
        super(exception);
    }
}
