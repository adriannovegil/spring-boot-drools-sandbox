package com.neo.drools.util.ruleengine;

import org.kie.api.runtime.KieContainer;

/**
 *
 * @author Adrian Novegil <adrian.novegil@gmail.com>
 */
public class KieUtils {

    public static final String RULES_PATH = "src/main/resources/rules/";
    public static final String PACKAGE_NAME = "com.neo.drools";
    
    private static KieContainer kieContainer;

    /**
     * 
     * @return 
     */
    public static KieContainer getKieContainer() {
        return kieContainer;
    }

    /**
     * 
     * @param kieContainer 
     */
    public static void setKieContainer(KieContainer kieContainer) {
        KieUtils.kieContainer = kieContainer;
    }
}
