package com.neo.drools.util.randomizer;

import static com.neo.drools.util.randomizer.Randomizer.randomInt;
import java.util.Calendar;

/**
 *
 * @author Adrian Novegil <adrian.novegil@gmail.com>
 */
public class InditexRandomizer extends Randomizer {

    /**
     * Generador de codigos de articulo aleatorios
     *
     * @return
     */
    public static String randomArticulo() {
        Integer first = randomInt(1000, 9000);
        Integer second = randomInt(100, 900);
        return String.valueOf(first) + "-" + String.valueOf(second);
    }

    /**
     * Metodo que genera una campanha de forma aleatoria
     *
     * @return
     */
    public static String randomCampanha() {
        String campanha = null;
        int index = randomInt(1, 2);
        char letra;
        if (index == 1) {
            letra = 'V';
        } else {
            letra = 'I';
        }
        int year = Calendar.getInstance().get(Calendar.YEAR);
        campanha = letra + String.valueOf(year % 100);
        return campanha;
    }

    /**
     *
     * @return
     */
    public static String randomEmpresa() {
        String empresa = null;
        int index = randomInt(1, 5);
        switch (index) {
            case 1:
                empresa = "INDITEX";
                break;
            case 2:
                empresa = "OYSHO";
                break;
            case 3:
                empresa = "BERSHKA";
                break;
            case 4:
                empresa = "MARACOVA";
                break;
            case 5:
                empresa = "TEMPE";
                break;
            default:
                break;
        }
        return empresa;
    }

    /**
     *
     * @return
     */
    public static String randomSeccion() {
        String seccion = null;
        int index = randomInt(1, 3);
        switch (index) {
            case 1:
                seccion = "Señora";
                break;
            case 2:
                seccion = "Caballero";
                break;
            case 3:
                seccion = "Niño";
                break;
            default:
                break;
        }
        return seccion;
    }

    /**
     *
     * @return
     */
    public static String randomIncoterm() {
        String seccion = null;
        int index = randomInt(1, 2);
        if (index == 1) {
            seccion = "CIF";
        } else if (index == 2) {
            seccion = "FOB";
        }
        return seccion;
    }

    /**
     *
     * @return
     */
    public static Integer randomShippingStatus() {
        Integer status = null;
        int index = randomInt(1, 5);
        switch (index) {
            case 1:
                status = 25;
                break;
            case 2:
                status = 50;
                break;
            case 3:
                status = 75;
                break;
            case 4:
                status = 100;
                break;
            case 5:
                status = 1;
                break;
            default:
                break;
        }
        return status;
    }

    /**
     *
     * @return
     */
    public static String randomDestino() {
        String destino = null;
        int index = randomInt(1, 12);
        switch (index) {
            case 1:
                destino = "ARTEIXO";
                break;
            case 2:
                destino = "OYSHO";
                break;
            case 3:
                destino = "TEMPE";
                break;
            case 4:
                destino = "NEWWEAR";
                break;
            case 5:
                destino = "MANEIRA";
                break;
            case 6:
                destino = "TEM-SUR";
                break;
            case 7:
                destino = "IND-RDC";
                break;
            case 8:
                destino = "TEM-RDC";
                break;
            case 9:
                destino = "TE-RD-S";
                break;
            case 10:
                destino = "NEW-OFT";
                break;
            case 11:
                destino = "ZARAGOZA";
                break;
            case 12:
                destino = "MECO";
                break;
            default:
                break;
        }
        return destino;
    }

}
