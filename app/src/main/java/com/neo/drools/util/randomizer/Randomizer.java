package com.neo.drools.util.randomizer;

import java.util.Arrays;
import java.util.Date;
import java.util.Random;

/**
 *
 * @author Adrian Novegil <adrian.novegil@gmail.com>
 */
public abstract class Randomizer {

    private static final String[] COLORS_ES = {"ROJO", "VERDE", "AZUL", "MAGENTA",
        "CIAN", "AMARILLO", "MARRÓN", "VIOLETA", "NARANJA", "BLANCO", "GRIS", "NEGRO"};
    private static final String[] COLORS_EN = {"RED", "GREEN", "BLUE", "MAGENTA",
        "CIAN", "YELLOW", "BROWN", "VIOLET", "ORANGE", "WHITE", "GREY", "BLACK"};
    private static final String[] SIZES = {"XS", "S", "M", "L", "XL"};
    // Random generator
    private static final Random random = new Random();

    /**
     * Metodo que genera un nombre aleatoreamente.
     *
     * @return Nombre generado.
     */
    public static String randomFirstName() {
        return randomString(2, 12);
    }

    /**
     * Metodo que genera el primer apellido aleatoreamente.
     *
     * @return Primer apellido generado.
     */
    public static String randomMiddleName() {
        return randomString(2, 12);
    }

    /**
     * Metodo que genera el segundo apellido aleatoreamente.
     *
     * @return Segundo apellido generado.
     */
    public static String randomLastName() {
        return randomString(6, 12);
    }

    /**
     * Metodo que genera aleatoreamente el nombre de una calle.
     *
     * @return Nombre de la calle generado.
     */
    public static String randomStreet() {
        return randomString(10, 20);
    }

    /**
     * Metodo que genera aleatroreamente el nombre de una ciudad.
     *
     * @return Nombre de la ciudad generado.
     */
    public static String randomCity() {
        return randomString(10, 20);
    }

    /**
     * Metodo que genera aleatoreamente el nombre de un estado.
     *
     * @return Nombre del estado generado.
     */
    public static String randomState() {
        return randomString(2, 2);
    }

    /**
     * Metodo que genera aleatoreamente el nombre de un pais.
     *
     * @return Nombre del pais generado.
     */
    public static String randomCountry() {
        return randomString(3, 15);
    }

    /**
     * Metdodo que genera automaricamente un codigo postal.
     *
     * @return Codigo postal generado.
     */
    public static String randomZip() {
        return random.nextBoolean()
                ? randomNumString(5, 5) : randomNumString(9, 9);
    }

    /**
     * Metodo que genera automaticamente un numero de telefono.
     *
     * @return Numero de telefono generado.
     */
    public static String randomPhone() {
        return randomNumString(15, 15);
    }

    /**
     * Metodo que genera automaticamente una cuenta de correo electronico.
     *
     * @return Cuenta de correo electronico generado.
     */
    public static String randomEmail() {
        return randomString(3, 8) + "@"
                + randomString(3, 8) + "." + randomString(2, 3);
    }

    /**
     *
     * @param minLength
     * @param maxLength
     * @return
     */
    public static String randomString(int minLength, int maxLength) {
        return randomString(randomInt(minLength, maxLength), 'A', 'Z');
    }

    /**
     *
     * @param minLength
     * @param maxLength
     * @return
     */
    public static String randomNumString(int minLength, int maxLength) {
        return randomString(randomInt(minLength, maxLength), '0', '9');
    }

    /**
     *
     * @param length
     * @param from
     * @param to
     * @return
     */
    public static String randomString(int length, char from, char to) {
        char[] chars = new char[length];
        for (int i = 0; i < length; i++) {
            chars[i] = (char) randomInt(from, to);
        }
        return new String(chars);
    }

    /**
     * Metodo que genera automaticamente un array de objetos tipo Date.
     *
     * @param count Numero de objetos del tipo Date que queremos generar.
     * @return Array de objetos tipo Date generado.
     */
    public static Date[] randomDates(int count) {
        Date[] dates = new Date[count];
        for (int i = 0; i < count; i++) {
            dates[i] = randomDate();
        }
        Arrays.sort(dates);
        return dates;
    }

    /**
     * Metodo que genera automaticamente un objeto de tipo Date.
     *
     * @return Objeto tipo Date generado.
     */
    public static Date randomDate() {
        long time = (random.nextLong() & 0x1FFFFFFFFFFL) - 0xFFFFFFFFFFL;
        return new Date(time); // 1935 - 2004 
    }

    /**
     * Metodo que genera automaticamente un numero entero, que puede ser
     * positivo o negativo.
     *
     * @param min Valor minimo que puede tomar el numero entero generado.
     * @param max Valor maximo que puede tomar el numero entero generado.
     * @return Numero entero generado.
     */
    public static int randomInt(int min, int max) {
        return max <= min ? min : (random.nextInt(max - min + 1) + min);
    }

    /**
     * Metodo que genera automaticamente un numero Long.
     *
     * @return Numero Long generado.
     */
    public static Long randomLong() {
        long leftLimit = 1L;
        long rightLimit = 10L;
        return leftLimit + (long) (Math.random() * (rightLimit - leftLimit));
    }

    /**
     * Metodo que genera automaticamente un numero entero positivo.
     *
     * @param min Valor minimo que puede tomar el numero entero generado.
     * @param max Valor maximo que puede tomar el numero entero generado.
     * @return Numero entero generado.
     */
    public static int randomPositiveInt(int min, int max) {
        return Math.abs(max <= min ? min : (random.nextInt(max - min + 1) + min));
    }

    /**
     * Metodo que genera automaticamente un valor booleano
     *
     * @return Valor booleano.
     */
    public static boolean randomBoolean() {
        return random.nextBoolean();
    }

    /**
     *
     * @return
     */
    public static String randomPedido() {
        String pedido = null;
        int index = randomInt(10000, 90000);
        String letra = randomString(1, 1);
        int seccion = randomInt(1, 9);
        pedido = index + "-" + letra + "/" + seccion;
        return pedido;
    }

    /**
     *
     * @return
     */
    public static String randomColorEs() {
        return COLORS_ES[randomInt(0, COLORS_ES.length - 1)];
    }

    /**
     *
     * @return
     */
    public static String randomColorEn() {
        return COLORS_EN[randomInt(0, COLORS_ES.length - 1)];
    }

    /**
     *
     * @return
     */
    public static String randomSize() {
        return SIZES[randomInt(0, SIZES.length - 1)];
    }
}
