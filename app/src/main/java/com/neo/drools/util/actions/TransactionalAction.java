package com.neo.drools.util.actions;

/**
 *
 * @author Adrian Novegil <adrian.novegil@gmail.com>
 * @param <T>
 */
public interface TransactionalAction<T> extends Action<T> {

    /**
     *
     * @return
     */
    public Boolean prepare();

    /**
     *
     * @param action
     * @return
     */
    public Boolean done(Integer action);

}
