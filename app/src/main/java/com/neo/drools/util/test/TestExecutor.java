package com.neo.drools.util.test;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Adrian Novegil <adrian.novegil@gmail.com>
 */
public final class TestExecutor {

    /**
     * Mapa de resultados de la ejecucion de las pruebas.
     */
    private static final Map<String, Long> timeResults = new HashMap<>();

    /**
     * Consturctor por defecto.
     */
    private TestExecutor() {
    }

    /**
     *
     * @param test
     * @return
     */
    public static Boolean process(Test test) {
        long startTime = System.currentTimeMillis();
        // Verificamos si falla la fase de configuracion
        if (!test.config()) {
            return false;
        }
        long configTime = System.currentTimeMillis();
        timeResults.put("config", configTime - startTime);
        // Verificamos si falla la fase de preparacion
        if (!test.prepare()) {
            return false;
        }
        long prepareTime = System.currentTimeMillis();
        timeResults.put("prepare", prepareTime - configTime);
        // Verificamos si falla la fase de ejecucion.
        if (!test.execute()) {
            return false;
        }
        long executeTime = System.currentTimeMillis();
        timeResults.put("execute", executeTime - prepareTime);
        // Verificamos si falla la fase de cierre.
        test.close();
        long closeTime = System.currentTimeMillis();
        timeResults.put("close", closeTime - executeTime);
        // Print results
        pritTimeResults();
        // Return true
        return true;
    }

    /**
     * Aux funtion to print the time results
     */
    private static void pritTimeResults() {
        System.out.println();
        System.out.println("=================================================");
        System.out.println(" Time results");
        System.out.println("=================================================");
        System.out.println();
        timeResults.keySet().forEach((name) -> {
            Long value = timeResults.get(name);
            System.out.println("Time for the stage " + name + ": " + value + " ms");
        });
        System.out.println();
        System.out.println("=================================================");
        System.out.println();
    }
}
