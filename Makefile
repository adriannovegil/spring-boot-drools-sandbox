# COLORS
GREEN  := $(shell tput -Txterm setaf 2)
WHITE  := $(shell tput -Txterm setaf 7)
YELLOW := $(shell tput -Txterm setaf 3)
RED := $(shell tput -Txterm setaf 1)
RESET  := $(shell tput -Txterm sgr0)

# REQUIRED SECTION
ROOT_DIR:=$(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))

# DEFAULT variables
DOCKER_COMPOSE := docker-compose
DOCKER_COMPOSE_FILE := $(ROOT_DIR)/docker-compose.yml

.PHONY: help dependencies up start stop restart status ps clean

help:
	@ echo
	@ echo '  Usage:'
	@ echo '    make <target> [flags...]'
	@ echo ''
	@ echo '  Targets:'
	@ echo '    make build              build all containers'
	@ echo '    make start              start all containers in interactive mode'
	@ echo '    make start c=hello      start the container hello in interactive mode'
	@ echo '    make daemon             start all containers as daemon'
	@ echo '    make daemon c=hello     start the container hello as daemon'
	@ echo '    make up                 build + start all containers'
	@ echo '    make restart            restart all containers'
	@ echo '    make stop               stop all containers'
	@ echo '    make status             show list of containers with statuses'
	@ echo '    make ps                 alias of status'
	@ echo '    make logs               show logs'
	@ echo '    make clean              clean containers'
	@ echo '    make clean-all          clean all data. Containers and volumes'
#	@ echo '    make push               push the images to the Docker repository'
#	@ echo '    make release            generate a new release'
	@ echo '    make help               show this help'
	@ echo ''
	@ echo '  Flags:'
	@ echo '    Nothing for the moment :-P'
	@ echo ''

confirm: ## Solicite confirm and action before exec it
	@( read -p "$(RED)Are you sure? [y/N]$(RESET): " sure && case "$$sure" in [yY]) true;; *) false;; esac )

build: ## Start all or c=<name> containers in foreground
	@$(DOCKER_COMPOSE) -f $(DOCKER_COMPOSE_FILE) build

start: ## Start all or c=<name> containers in background
	@$(DOCKER_COMPOSE) -f $(DOCKER_COMPOSE_FILE) up $(c)

daemon: ## Start all or c=<name> containers in background
	@$(DOCKER_COMPOSE) -f $(DOCKER_COMPOSE_FILE) up -d $(c)

up: build start ## Start all or c=<name> containers in foreground

restart: ## Restart all or c=<name> containers
	@$(DOCKER_COMPOSE) -f $(DOCKER_COMPOSE_FILE) stop $(c)
	@$(DOCKER_COMPOSE) -f $(DOCKER_COMPOSE_FILE) up $(c) -d

stop: ## Stop all or c=<name> containers
	@$(DOCKER_COMPOSE) -f $(DOCKER_COMPOSE_FILE) stop $(c)

status: ## Show status of containers
	@$(DOCKER_COMPOSE) -f $(DOCKER_COMPOSE_FILE) ps

logs: ## Show logs for all or c=<name> containers
	@$(DOCKER_COMPOSE) -f $(DOCKER_COMPOSE_FILE) logs --tail=100 -f $(c)

ps: status ## Alias of status

clean: confirm ## Clean all data
	@$(DOCKER_COMPOSE) -f $(DOCKER_COMPOSE_FILE) down

clean-all: confirm ## Clean all data and volumes
	@$(DOCKER_COMPOSE) -f $(DOCKER_COMPOSE_FILE) rm
	@$(DOCKER_COMPOSE) -f $(DOCKER_COMPOSE_FILE) down -v

#push: confirm ## Clean all data
#	@$(DOCKER_COMPOSE) -f $(DOCKER_COMPOSE_FILE) down

#release: confirm ## Clean all data and volumes
#	@$(DOCKER_COMPOSE) -f $(DOCKER_COMPOSE_FILE) down

default: help
