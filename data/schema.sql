/*
 Navicat MySQL Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50716
 Source Host           : localhost
 Source Database       : test

 Target Server Type    : MySQL
 Target Server Version : 50716
 File Encoding         : utf-8

 Date: 07/31/2017 19:11:43 PM
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- CREATE DATABASE IF NOT EXISTS `drools-sandbox`;
USE `db`;

-- ----------------------------
--  Table structure for `rule`
-- ----------------------------
DROP TABLE IF EXISTS `rule`;
CREATE TABLE `rule` (
  `id` bigint(20) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  `content` varchar(2048) NOT NULL,
  `catalog` varchar(255) NOT NULL,
  `create_time` varchar(255) NOT NULL,
  `last_modify_time` varchar(255) DEFAULT NULL,
  `rule_key` varchar(255) NOT NULL,
  `version` varchar(255) NOT NULL
);

-- ----------------------------
--  Records of `rule`
-- ----------------------------
-- BEGIN;
-- INSERT INTO `rule` VALUES ('1', 'package com.neo.drools\n\nimport com.neo.drools.model.domain.Address;\nimport com.neo.drools.model.domain.fact.AddressCheckResult;\n\nrule \"Postcode 6 numbers\"\n\n    when\n        address : Address()\n        checkResult : AddressCheckResult();\n    then\n        checkResult.setPostCodeResult(true);\n		System.out.println(\"Registro：OK!\");\nend', '111', '111', 'score', '1');
-- COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
